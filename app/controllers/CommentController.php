<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//-- Get variables through form data
		$postid = Input::get('postid');
		$user = Auth::user()->id;;
		$message = Input::get('message');
		
		$result = insertComment($postid, $user, $message);
		
		if ($result) 
		{
			return Redirect::to(url('post/'.$postid)); //-- Go to posts / comments page
		} 
		else
		{
			die("Please fill out all form fields."); //-- If bypass javascript validation
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$postid = deleteComment($id);
		
		if ($postid)
		{
			return Redirect::to(url('post/'.$postid)); //-- Go to posts / comments page
		} 
		else
		{
			die("Error deleting comment."); //-- If bypass javascript validation
		}
	}
	
}

function insertComment($postid, $user, $message) 
{
	$id = 0;
	
	//-- Make sure form input not interpreted as HTML
	$postid = htmlspecialchars($postid);
	$user = htmlspecialchars($user);
	$message = htmlspecialchars($message);
	
	//-- Make sure form elements not blank
	if ($postid != '' && $user != '' && $message != '')
	{
		$comment = new Comment;

		$comment->post_id = $postid;
		$comment->user_id = $user;
		$comment->message = $message;
		
		$comment->save();
		
		$id = 1;
	}
	
	return $id;
}

function deleteComment($id)
{
	//-- Get post id to redirect to after deleting comment
	$comment = Comment::find($id);
	$postid = $comment->post_id;
	
	if ($comment->user_id == Auth::user()->id)
	{
		$comment->delete();
	}
  	
	return $postid;
}