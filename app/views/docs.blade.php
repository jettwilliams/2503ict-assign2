@extends('layouts.master')

@section('title')
Generic Social Network - Edit Post
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Documentation</div>
		<p>Everything in the assignment task sheet was implemented in full except for uploading and displaying images. Also, when filling in forms incorrectly, the error messages are not as helpful as they could be. It took a bit more time and was more difficult than the first half of the assignment.</p>
		<p>I have taken a few interesting approaches in regards to implementing features. Forms are validated first using javascript and the submit button is disabled until the form is valid. Of course they are also validated through PHP to stop people bypassing the JavaScript. All of the styling is my own CSS and I have not used bootstrap. A users friends are shown on their profile page. To get posts from friends, a user's friend list is created, then the list is used in a single eloquent query.</p>
		
		<div style="padding-top: 10px; padding-bottom: 10px;" class="title colored">Entity-Relationship Diagram</div>
		<img src="/2503ict-assign2/public/images/erd2.svg" alt="Entity-Relationship Diagram" />
		
		<div style="padding-top: 20px;" class="title colored">Site Diagram</div>
		<img src="/2503ict-assign2/public/images/site2.svg" alt="Site Diagram" />
		
	</div>
@stop
