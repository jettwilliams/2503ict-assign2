@extends('layouts.master')

@section('title')
Generic Social Network - Account Settings
@stop

@section('content')
	<div class="timeline">
		
		<div class="title colored">Account Settings</div>
		
		<div class="create">
			@if (Auth::check())
				{{ Form::open(array('action' => 'UserController@update', 'files' => true)) }}
					<input name="fname" type="text" placeholder="Name"  value="{{{ Auth::user()->name }}}" onkeyup="validateForm()" />
					<input name="email" type="email" placeholder="Email" value="{{{ Auth::user()->email }}}" onkeyup="validateForm()" />
					<input name="dob" type="date" value="{{{ Auth::user()->birthdate }}}" onkeyup="validateForm()" onchange="validateForm()" />
					<span style="margin-top: 10px;">Profile Image:</span> <input style="margin-top: 10px;" name="image" type="file" accept="image/*" />
					
					<div id="submit" class="button" onclick="submitForm()">Save</div>
				{{ Form::close() }}
				
				<!--{{ Form::open(array('action' => 'UserController@update')) }}
					<input name="pass0" type="password" placeholder="Old password" onkeyup="validateForm(2)" />
					<input name="pass1" type="password" placeholder="New password" onkeyup="validateForm(2)" />
					<input name="pass2" type="password" placeholder="Re-enter new password" onkeyup="validateForm(2)" />
					<div id="submit2" class="button" onclick="submitForm(2)">Save</div>
				{{ Form::close() }}-->
			@else
				<a href="/2503ict-assign2/public/login">Log in</a> or <a href="/2503ict-assign2/public/signup">Sign up</a> to view your account.
			@endif
		</div>
		
	</div>
@stop
