<?php

class CommentSeeder extends Seeder {
    public function run()
    {
        for ($i=1; $i<=10; $i++)
        {
            for ($j=0; $j<2; $j++)
            {
                $comment = new Comment;
                $comment->post_id = 1+(($i-1)*$j);
                $comment->user_id = 1+(($i-1)*$j);
                $comment->message = 'This is a test comment ' . $i;
                $comment->save();
            }
        }
    }
}